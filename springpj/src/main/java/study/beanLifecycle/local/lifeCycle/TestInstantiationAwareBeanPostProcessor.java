package study.beanLifecycle.local.lifeCycle;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

public class TestInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    static Logger log = Logger.getLogger("BeanLifeCycleTest");

    public TestInstantiationAwareBeanPostProcessor() {
        super();
        log.info("TestInstantiationAwareBeanPostProcessor + 实例化");
    }

    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        log.info("InstantiationAwareBeanPostProcessor.postProcessBeforeInstantiation+所有Bean实例化之前");
        return null;
    }

    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        log.info("InstantiationAwareBeanPostProcessor.postProcessBeforeInstantiation+所有Bean实例化之后");
        return true;
    }

    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        // postProcessAfterInstantiation 返回true，该方法执行
        log.info("返回true，该方法执行");
        return null;
    }
}
