package study.beanLifecycle.local.lifeCycle;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;


public class AuthorInitialBean implements BeanFactoryAware,InitializingBean, DisposableBean, BeanNameAware {
    public static Logger log = Logger.getLogger("AuthorInitialBean");
    private String name="默认值";
    private String beanName;
    private Integer age;
    private BeanFactory beanFactory;
    public AuthorInitialBean() {
        log.info("AuthorInitialBean+Bean实例化");
        //this.name = "实例化";
    }
    public void afterPropertiesSet() throws Exception {
        log.info("afterPropertiesSet+InitializingBean 初始化");
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        log.info("setName + Bean属性赋值");
        this.name = name;
    }

    public void destroy() throws Exception {
        log.info("DisposableBean.destroy");
    }

    public void authorInitialBeanDestory(){
        log.info("init-method + Bean初始化");

    }

    public void setBeanName(String beanName) {
        log.info("Bean+BeanNameAware.setBeanName");
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        log.info(" Bean+ BeanFactoryAware.setBeanFactory");
        this.beanFactory = beanFactory;
    }

    @Override
    public String toString() {
        return "name:"+this.getName()+",age:"+this.getAge();
    }
}
