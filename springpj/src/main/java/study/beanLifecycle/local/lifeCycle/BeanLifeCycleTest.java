package study.beanLifecycle.local.lifeCycle;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifeCycleTest {
    static Logger log = Logger.getLogger("BeanLifeCycleTest");
    public static void main(String[] args) {

        ApplicationContext factory = new ClassPathXmlApplicationContext("application-beans.xml");
        System.out.println("======");
        AuthorInitialBean bean = factory.getBean("authorInitialBean", AuthorInitialBean.class);
        System.out.println(bean.toString());
        ((ClassPathXmlApplicationContext) factory).registerShutdownHook();
    }

}


