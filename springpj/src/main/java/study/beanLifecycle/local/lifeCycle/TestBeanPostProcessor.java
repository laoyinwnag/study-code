package study.beanLifecycle.local.lifeCycle;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class TestBeanPostProcessor implements BeanPostProcessor {
    public static Logger log = Logger.getLogger("AuthorInitialBean");

    public TestBeanPostProcessor() {
        super();
        log.info("TestBeanPostProcessor(BeanPostProcessor)+实例化");
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.info("BeanPostProcessor.postProcessBeforeInitialization+所有Bean初始化之前");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        log.info("BeanPostProcessor.postProcessAfterInitialization+所有Bean初始化之后");
        return bean;
    }
}
