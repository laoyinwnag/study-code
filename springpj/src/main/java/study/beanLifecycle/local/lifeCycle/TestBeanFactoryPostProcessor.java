package study.beanLifecycle.local.lifeCycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.logging.Logger;

public class TestBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    Logger log = Logger.getLogger("GPBeanFactoryPostProcessor");
    public TestBeanFactoryPostProcessor() {
        super();
        log.info("BeanFactoryPostProcessor + 实例化！");
    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        log.info("BeanFactoryPostProcessor调用postProcessBeanFactory方法");
        BeanDefinition bd = configurableListableBeanFactory.getBeanDefinition("authorInitialBean");
        bd.getPropertyValues().addPropertyValue("age", "16");
    }
}

