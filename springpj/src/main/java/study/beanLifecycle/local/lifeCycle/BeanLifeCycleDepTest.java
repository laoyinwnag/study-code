package study.beanLifecycle.local.lifeCycle;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import study.beanLifecycle.local.cycleDp.AuthorInitialBeanCycle;
import study.beanLifecycle.local.cycleDp.SecondBean;

public class BeanLifeCycleDepTest {
    static Logger log = Logger.getLogger("BeanLifeCycleTest");
    public static void main(String[] args) {

        ApplicationContext factory = new ClassPathXmlApplicationContext("application-beans.xml");
        System.out.println("======");
        AuthorInitialBeanCycle bean = factory.getBean("authorInitialBeanCycle", AuthorInitialBeanCycle.class);
        System.out.println(bean.toString());
        SecondBean secondBean = factory.getBean("secondBean", SecondBean.class);
        System.out.println(secondBean.toString());

        ((ClassPathXmlApplicationContext) factory).registerShutdownHook();
    }

}


