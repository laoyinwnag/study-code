package study.beanLifecycle.local.cycleDp;

import org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CycleDpTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(ScanPackageConfig.class);
        AbstractAutowireCapableBeanFactory beanFactory = (AbstractAutowireCapableBeanFactory) context.getBeanFactory();
        beanFactory.setAllowCircularReferences(true);
        context.refresh();
        AuthorInitialBeanCycle simpleBean = (AuthorInitialBeanCycle) context.getBean("authorInitialBeanCycle");
        //simpleBean.setAge(18);
        System.out.println(simpleBean.toString());
    }
}
