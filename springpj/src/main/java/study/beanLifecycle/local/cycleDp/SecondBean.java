package study.beanLifecycle.local.cycleDp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Component
public class SecondBean {
    public static Logger log = Logger.getLogger("SecondBean");
    private String name="secondBean";

    @Autowired
    protected AuthorInitialBeanCycle authorInitialBeanCycle;

    public SecondBean() {
        log.info("SecondBean实例化");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name:" + this.getName();
    }
}
