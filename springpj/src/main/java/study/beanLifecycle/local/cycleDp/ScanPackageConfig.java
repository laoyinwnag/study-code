package study.beanLifecycle.local.cycleDp;

import org.springframework.context.annotation.ComponentScan;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("study.beanLifecycle.local.cycleDp")
public class ScanPackageConfig {

}
