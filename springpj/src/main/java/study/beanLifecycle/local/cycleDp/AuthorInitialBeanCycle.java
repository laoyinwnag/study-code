package study.beanLifecycle.local.cycleDp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;


@Component
public class AuthorInitialBeanCycle {
    public static Logger log = Logger.getLogger("AuthorInitialBeanCycle");
    private String name = "authorInitialBeanCycle";
    private Integer age;

    @Autowired
    protected SecondBean secondBean;

    public AuthorInitialBeanCycle() {
        log.info("AuthorInitialBean+Bean实例化");
        //this.name = "实例化";
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        log.info("setName + Bean属性赋值");
        this.name = name;
    }

    @Override
    public String toString() {
        return "name:"+this.getName() + "";
    }
}
