package study.beanLifecycle.local.Aop;

import org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class CycleAopTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(ScanPackageAopConfig.class);
        AbstractAutowireCapableBeanFactory beanFactory = (AbstractAutowireCapableBeanFactory) context.getBeanFactory();
        beanFactory.setAllowCircularReferences(true);
        context.refresh();
        AuthorInitialBeanCycle simpleBean = (AuthorInitialBeanCycle) context.getBean("authorInitialBeanCycle");
        //simpleBean.setAge(18);
        simpleBean.toString();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
