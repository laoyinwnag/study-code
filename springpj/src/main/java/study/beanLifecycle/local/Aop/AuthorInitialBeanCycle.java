package study.beanLifecycle.local.Aop;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import study.beanLifecycle.local.cycleDp.SecondBean;


@Component
public class AuthorInitialBeanCycle {
    public static Logger log = Logger.getLogger("AuthorInitialBeanCycle");
    private String name = "authorInitialBeanCycle";
    private Integer age;

    public AuthorInitialBeanCycle() {
        log.info("AuthorInitialBean+Bean实例化");
        //this.name = "实例化";
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        log.info("setName + Bean属性赋值");
        this.name = name;
    }

    @Override
    public String toString() {
        System.out.println("name:"+this.getName());
        return "name:"+this.getName() + "";
    }
}
