package study.beanLifecycle.local.Aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectJAop {

    @Pointcut("execution(* study.beanLifecycle.local.Aop.AuthorInitialBeanCycle.*(..))")
    private void point() {}

    @Before("point()")
    public void beforeAspect(JoinPoint points) {
        System.out.println("执行的方法签名："+points.getSignature());
    }

    @After("execution(* study.beanLifecycle.local.Aop.AuthorInitialBeanCycle.*(..))")
    public void afterAspect(){
        System.out.println("afterAspect");
    }

}

