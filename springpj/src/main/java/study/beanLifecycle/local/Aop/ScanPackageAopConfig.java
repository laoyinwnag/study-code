package study.beanLifecycle.local.Aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(exposeProxy = true)
@ComponentScan("study.beanLifecycle.local.Aop")
public class ScanPackageAopConfig {
}
