package study.beanLifecycle.orign;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TestBean {
    private String name;
    private Integer age;
}
