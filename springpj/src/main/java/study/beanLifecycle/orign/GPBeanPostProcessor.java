package study.beanLifecycle.orign;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class GPBeanPostProcessor implements BeanPostProcessor {
    static Logger log = Logger.getLogger("BeanLifeCycleTest");
    public GPBeanPostProcessor(){
        log.info("调用BeanPostProcessor实现类构造器！！");
    }
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        log.info("BeanPostProcessor接口方法postProcessBeforeInitialization对属性进行更改");
        return o;
    }

    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        log.info("BeanPostProcessor接口方法postProcessAfterInitialization对属性进行更改");
        return o;
    }
}

