package study.beanLifecycle.orign;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import study.beanLifecycle.local.lifeCycle.AuthorInitialBean;

public class BeanLifeCycleTest {
    static Logger log = Logger.getLogger("BeanLifeCycleTest");
    public static void main(String[] args) {
        log.info("====== 开始初始化Spring容器 ========");

        ApplicationContext factory = new ClassPathXmlApplicationContext("application-beans.xml");

        AuthorInitialBean bean = factory.getBean("authorInitialBean", AuthorInitialBean.class);
        bean.setName("sdfasdf");
        System.out.println(bean);
        /* log.info("====== 初始化Spring容器成功 ========");

        //获取Author实例
        Author author = factory.getBean("author", Author.class);

        log.info(author.toString());

        log.info("====== 开始销毁Spring容器 ========");*/

        ((ClassPathXmlApplicationContext) factory).registerShutdownHook();
    }

}


